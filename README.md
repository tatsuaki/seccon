2015年度プロジェクト活動：SECCON班
========================
これは，2015年度プロジェクト活動のＳＥＣＣＯＮ班のリポジトリです．
文書は，LaTeXを用いて執筆します．

【執筆ルール】
===
１何年の何の課題か
「SECCON CTF 2014 オンライン予選」
「このパケットを解析せよ（ネットワーク 100）」

２課題の内容

４解法

５使ったツールについてとか

ソースコードなど外部ファイルを読み込む

```
\renewcommand{\lstlistingname}{ソースコード}
\lstinputlisting[{language=C},caption=キャプション,label=file_name]
{file_name.c}
```


文章表現について
===
文章表現について統一表記をここに示します．

口調
---
である調

だ調はやめて下さい．

だ調 		だ 	でない 	せぬ 	ならぬ 	だろう 	だから

である調 	である 	でない 	しない 	ならない 	であろう 	であるから

数字
---
西暦などの固有名詞系以外は漢数字とする

西暦等
---
半角のアラビア数字

以下省略
---
正式名称(以下，○○)

句読点
---
，．(それぞれ全角)

カンマ・ピリオドも全角でお願いします

本来半角でなければならい物除く(例：ファイル名))

括弧
---
()(それぞれ半角)

カギカッコ
---
「」(『』や""は「」へ)

英数字
---
原則半角

固有名詞の表記
---
ロゴなどを基準とする