PLATEX = platex
DVIPDFMX = dvipdfmx

PDF_READER = evince

TARGET = document
OUTPUT_DIR = build
BUILDED_TARGET = $(OUTPUT_DIR)/$(TARGET)
SOURCE_DIR = src
SOURCE_TARGET = $(SOURCE_DIR)/$(TARGET)

all: $(BUILDED_TARGET).dvi

pdf: $(BUILDED_TARGET).pdf

$(BUILDED_TARGET).dvi: $(SOURCE_TARGET).tex
	mkdir -p $(OUTPUT_DIR)
	$(PLATEX) -interaction=nonstopmode -output-directory=$(OUTPUT_DIR) $(SOURCE_TARGET).tex
	$(PLATEX) -interaction=nonstopmode -output-directory=$(OUTPUT_DIR) $(SOURCE_TARGET).tex

$(BUILDED_TARGET).pdf: $(BUILDED_TARGET).dvi
	$(DVIPDFMX) -o $(BUILDED_TARGET).pdf $(BUILDED_TARGET).dvi

allclean: clean
	cd $(OUTPUT_DIR) && rm -rf $(TARGET).pdf

clean:
	cd $(OUTPUT_DIR) && rm -rf *.dvi *.log *.aux *.toc

open: pdf
	$(PDF_READER) $(BUILDED_TARGET).pdf &

re: allclean open
